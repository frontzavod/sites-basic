var flickityConfig = {
	cellAlign: 'left',
	contain: true,
	imagesLoaded: true,
	wrapAround: true,
	pageDots: false
}

$('[type="tel"]').mask('+7(999) 999-9999');

ymaps.ready(function(){
	var center = [59.85987206428172,30.287930499999998];
	var map = new ymaps.Map('map',{center: center, zoom: 13})
	map.geoObjects.add(new ymaps.Placemark(center));
});